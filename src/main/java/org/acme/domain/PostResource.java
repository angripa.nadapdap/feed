package org.acme.domain;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.acme.model.dto.PostDto;
import org.acme.service.PostService;

@Path("/api/post")
public class PostResource {

   @Inject
   PostService postService;

   @GET
   @Path("/{id}")
   @Produces("application/json")
   public Response get(@PathParam("id") Long id) {
      return Response.ok(this.postService.findById(id)).build();
   }

   @POST
   @Produces("application/json")
   public Response create(PostDto postDto) {
      return Response.ok(this.postService.save(postDto)).build();
   }

   @DELETE
   @Path("/{id}")
   @Produces("application/json")
   public Response delete(@PathParam("id") Long id) {
      this.postService.deleteById(id);
      return Response.noContent().build();
   }

   @PUT
   @Path("{id}")
   @Produces("application/json")
   public Response update(@PathParam("id") Long id,PostDto postDto) {
      this.postService.update(id,postDto);
      return Response.noContent().build();
   }
   

}
