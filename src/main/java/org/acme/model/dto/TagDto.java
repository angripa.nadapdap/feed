package org.acme.model.dto;

import java.util.ArrayList;
import java.util.List;

public class TagDto {
    private String label;
    private List<PostDto> posts = new ArrayList<>();
    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }
    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }
    /**
     * @return the posts
     */
    public List<PostDto> getPosts() {
        return posts;
    }
    /**
     * @param posts the posts to set
     */
    public void setPosts(List<PostDto> posts) {
        this.posts = posts;
    }

    
}
