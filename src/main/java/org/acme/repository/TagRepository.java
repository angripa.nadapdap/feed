package org.acme.repository;



import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.acme.model.entity.Tag;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class TagRepository implements PanacheRepository<Tag>{
    public List<Tag> findALlByLabel(String label){
        return list("label", label);
    }

    public Tag findByLabel(String label){
        return find("label", label).firstResult();
    }
}