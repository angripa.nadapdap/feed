package org.acme.service;

import java.util.List;

import org.acme.model.dto.PostDto;
import org.acme.model.entity.Post;

public interface PostService {
    List<Post> findAll();
	Post findById(final Long id);
	Post save(final PostDto post);
	void update(final Long id,final PostDto postdDto);
	void deleteById(final Long id);
}
