package org.acme.service;

import org.acme.model.dto.TagDto;

public interface TagService {
	TagDto findByLabel(final String label);
}
