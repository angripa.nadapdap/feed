package org.acme.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.acme.model.dto.PostDto;
import org.acme.model.entity.Post;
import org.acme.model.entity.Tag;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.acme.service.PostService;

@ApplicationScoped
public class PostServiceImpl implements PostService {

    @Inject
    PostRepository postRepository;

    @Inject
    TagRepository tagRepository;

    @Transactional
    @Override
    public void deleteById(Long id) {
        Post p = postRepository.findById(id);
        if (p == null) {
            return;
        }
        for (Tag tag : p.getTags()) {
            tagRepository.delete(tag);
        }
        postRepository.deleteById(id);
    }

    @Override
    public List<Post> findAll() {
        return postRepository.listAll();
    }

    @Override
    public Post findById(Long id) {
        return postRepository.findById(id);
    }

    @Transactional
    @Override
    public Post save(PostDto post) {
        Set<Tag> tags = new HashSet<>();
        for (String tag : post.getTags()) {
            Tag t = tagRepository.findByLabel(tag);
            if (t != null) {
                tags.add(t);
            } else {
                t = new Tag();
                t.setLabel(tag);
            }
            tags.add(t);
        }
        tagRepository.persist(tags);

        Post nPost = new Post();
        nPost.setContent(post.getContent());
        nPost.setTitle(post.getTitle());
        nPost.setTags(tags);
        postRepository.persist(nPost);
        return nPost;
    }

    @Transactional
    @Override
    public void update(Long id, PostDto postDto) {
        Post p = postRepository.findById(id);
        if (p != null) {
            p.setContent(postDto.getContent());
            p.setTitle(postDto.getTitle());

            postRepository.persist(p);
        }

    }

}
