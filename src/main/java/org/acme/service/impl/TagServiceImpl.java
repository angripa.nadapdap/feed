package org.acme.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.acme.model.dto.PostDto;
import org.acme.model.dto.TagDto;
import org.acme.model.entity.Post;
import org.acme.model.entity.Tag;
import org.acme.repository.TagRepository;
import org.acme.service.TagService;

@ApplicationScoped
public class TagServiceImpl  implements TagService{

    @Inject
    TagRepository tagRepository;

    @Override
    public TagDto findByLabel(String label) {
        Tag tag = tagRepository.findByLabel(label);
        if(tag != null){
            TagDto tagDto = new TagDto();
            tagDto.setLabel(label);
            for(Post p : tag.getPosts()){
                PostDto postDto = new PostDto();
                postDto.setId(p.getId());
                postDto.setContent(p.getContent());
                postDto.setTitle(p.getTitle());
                tagDto.getPosts().add(postDto);
            }
            return tagDto;
        }
        return null;
    }
    
}
